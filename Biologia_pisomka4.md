# Biologia

1. **Pojmy:** <br>
osteocyt: neaktivna bunka kostneho tkaniva <br>
chondrocyt: neaktivna bunka chrupky <br>
fibrocyt: neaktivna bunka vaziva <br>
osteoblast: aktivna bunka kostneho tkaniva <br>
tela fibrosa: vazivo, tvorene z fibrocytov, vyplne, slachy, puzdra organov <br>
os: kost <br>
cartilago: chrupka, tvorena z chondrocytov - pruzna, odolna voci tlaku, bezcievna <br>
columna vertebalis: chrbtica <br>
chorda dorsalis: chrbtová struna (predchodca kostry (embryonalny vyvin, zatlacena chrupavkov)), kopijovce <br>
*cyt vs *blast - *cyt je neaktivna bunka a *blast je aktivna bunka <br>

2. **Medzibunkova hmota vaziva:** <br>
kyselina hyaluronova, kolagenove a elastinove vaziva

3. **Typ spojoveho tkaniva:** <br>
slacha: vazivo, casti rebier v spojeni s hrudnou kostou: hyalinna chrupka, klbove puzdro: vazivo, klbove plochy: hyalinna chrupka, usnica: elasticka chrupka, medzistavcove platnicky: vazivova chrupka, hrtanova prichlopka: elasticka chrupka, lonova spona: vazivova chrupka, rastove zony v dlhych kostiach: chrupka

4. **Aku funkciu ma okostica? Kde sa nachadza?** <br>
Pevna blana na povrchu kosti, vazivova struktura. Pokryva kost na miestach, kde nie su upony/blany.
Funkcia: výživa + rast kosti do hrúbky v prípade zlomeniny alebo tlaku (kosťotvorné bunky)

5. **Ake typy kostneho tkaniva ma dlha kost? Stavba dlhej kosti**
Huste a hubovite tkanivo.
Stavba (zhora dole): blizsia epifyza, rastova chrupka, hubovite kostne tkanivo, huste kostne tkanivo, drenova dutina (vnuri), krvna cieva (vnutri), diafyza (stredna cast), vzdialenejsia epifyza

6. **Aku funkciu ma rastova zona v dlhych kostiach** <br>
Zeby rast? do dĺžky

7. **!Priklady dlhych, kratkych a plochych kosti.** <br>
Dlhe kosti: stehenna, laktova, ramenna, vretenna, pistala, ihlica <br>
Kratke kosti: Zapastne, zaprstne, clanky prstov, priehlavkova, podpriehlavkova
Ploche kosti: lopatka, rebra, vsetko ostatne

8. **Co su "svy"?** <br>
Tiez aj sutury. Osifikovane rozhrania  medzi kostami lebky, pozname: vercovity, sipovity, lambda a 2 supinate.

9. **Kolko stavcov ma chrbtica** <br>
7 krcnych, 12 hrudnych, 5 driekovych, 5 krizovych (zrastene), 5 kostrcovych (zrastene)

10. **Ako sa volaju prve 2 krcna stavce? Aku maju funkciu??**
Nosic a capovec. Chyba im telo, kedze nemusia niest tazku vahu. Drzia nam hlavu

11. **Aky je rozdiel medzi kostnou drenou u deti a dospelych?** <br>
u deti vsade, u dospelych stukovatie a nachadza sa len v plochych kostiach

12. **!Ktore kosti tvoria mozgovu a ktore tvarovu cast lebky?** <br>
Mozgova - celova, temenna, zahkavna, cuchova, klinova, spankova <br>
Tvarova - calust, sanka, jarmova kost, cerieslo, nosova, slizna, jazykla, podnebna

13. **Ktore kosti tvoria tvoria pletenec hornej a dolnej koncatiny?** <br>
Pletenec - prepojenie koncatiny z chrbticou; horny: lopatka, klucna kost; dolny: panvove kosti

14. **Ktore kosti vytvaraju panvu?** <br>
bedrova lonova sedacia raz vpravo raz vlavo prepojene lonovou sponou

15. **Ake casti ma hrudna kost?** <br>
rukovet, telo, mecovity vybezok

16. **Telo stavca, otvor stavca** <br>
Telo - drzi chrbticu (miesta, na kt. posobi vacsia vaha , horne stavce telo nemaju), spojenie stavcov <br>
Otvor - miesto, aby mohla byt vedena miecha

17. **Aky typ spojivoveho tkaniva zabezpecuje spojenie stavcov medzi oblukmi?** <br>
medzistavcova platnicka na odpruzenie, pozdlzne vazy na pozdlzne udrziavanie

18. **Aky typ spojivoveho tkaniva zabezpecuje spojenie krizovych stavcov a krizovej kosti?** <br>
kostne tkanivo

19. **Klb kolena** <br>
vazy spajaju kosti a drzia polohu <br>
menisky odpruzuju klb

20. **Choroby** <br>
osteoporoza: riedke kosti = ubytok kostnej hmoty, lahsie sa lamu, prubudaju anorg. latky <br>
artroza: problem s klbam = opotrebovanie klbovej chrupavky,  bolest klbov <br>
skolioza: vychylenie chrbtice <br>
hyperlordoza: prilis velky predklon v driekovej casti chrbtice <br>
hyperkyfoza: prilis velke na vrchu hrudnej casti

21. **Osifikacia** <br>
dezmogenna a chondrogenna <br>
dezmogenna (vazivova) zo stredu von - fibroblasty sa zmenia najprv na osteoblasty-> osteocyty <br>
chondrogenna (chrupavkova()) - najprv sa dezmogenne osifikuje okostice, kt. ohranicuje kost, potom zo stredu von postupuje chondrogenne, nastava tam apoptoza samovrazda buniek (chondrocytov), nahradia sa osteocytmi