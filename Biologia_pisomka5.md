<<<<<<< HEAD

# Biologia 5

1. **Druhy svalového tkaniva + príklady** <br>
   srdcovy sval (myokard), priecne pruhovany sval (biceps), hladky sval (močový mechúr)

2. **Porovnaj štruktúru / stavbu jednotlivých buniek svalového tkaniva (podrobne) a načrtni dané bunky (v pozdĺžnom / priečnom reze)** <br>
   **PP sval** - má podlhovastý tvar (spageta) a pri pohlade je vidiet priecne pruhovanie, zakladna stavebna jednotka je myocyt ide o mnohojadrove svalove vlakno <br>
   ked si spravime priecny rez, vidime **sarcolemmu**, ktora to cele lemuje, **sarkoplazmu** v ktorej sa to cele lochni a spini (ohovaralo, klamalo, skodilo inym recami, hentam dalej, tam su vrzadeni, duseni...), potom vnutri su dalsie podlhovaste tuby (vyzera to ako tobogan) - **sarkomera = aktinomyozinovy komplex**, jednotlive vlakna sarkomery su myoribrily <br>
   tieto tuby su ohranicene nejakou sietkou - **sarkoplazmaticke retikulum** a potom tam este je **mitochondia**, kt. priecny rez vyzera ako graf sinusovej funkcie... Este tam je viac jadier <br>
   **Hladky sval** - ved o nom mame 3 riadky v prezentacii, poprosim doplnit, tvar vretienka, pri pozdlznom reze vidime nejake kliky-haky s bodkami - **sarkomera = aktinomyozinovy komplex** a modru elipsu, co je jadro - je len jedno <br>
   ked od vas sona bude chciet kontrahovany stav, nakreslite Tatry a pleso - vyzera to rovnako, len trochu menej spicate <br>
   svaly nevieme ovladat: ovladaju ich hormony <br>
   **Myokard** - o nom ani tie 3 riadky nemame... porposim doplnit, vyzera ako sliz s 1 jadrom,
   myokard je kombináciou hladkého a priečne pruhovaného takže niečo medzi

3. **Vysvetli význam sarkoméry buniek. Z čoho sa sarkoméra skladá? Podrobne túto jednotku popíš.** <br>
   umožňuje kontrakciu aktinomyozinovy komplex skladá sa z aktínu a myozínu (vlákna, fibrily) to je celé viac netreba dufam

4. **Popíš usporiadanie sarkoméry v jednotlivých svalových bunkách (jednotlivých svalových tkanív).** <br>
   no v hladkých svaloch cikcakovito usporiadané, v priečne pruhovaných rovnobežne, v myokarde rovnobežne

5. **Vysvetli význam sarkoplazmatického retikula pre bunku.** <br>
   dodáva vápnik pri kontrakcii svalu a ešte tam bola nejaká výživa myslím to ešte doplnim možno, žmýka zo seba vápnik

6. **Vysvetli ako reaguje sval na podnet?** <br>
   no to bolo také nejaké to s tou synaptickou štrbinou nie veď to je v ďalšej otázke popísané celé

7. **Vysvetli princíp svalových kontrakcií (podrobne – aj so synapsiou s neurónom, neurotransmitery atď.** <br>

   - nerv da svalu podnet na stiahnutie
   - nerv vysle na svalovu bunku neurotransmiter - acetylcholin
     - acetylcholin sa nachadza do vezikulach neuronu, tie idu na okraj nervovej bunky a vysypu neurotransmiter do synaptickej strbiny
   - svalova bunka ma na povrchu receptory a tie prijmu neurotransmiter, kt. aktivuje akcny potencial (akcny potencial vznika v mozgu a prenasa sa nim informacia??), kt. vypusti vapnik to sarkoplazmatickeho retikula
     - receptory -> neurotransmiter -> akcny potencial -> vapnik (zo sarkoplazmatickeho retikula)
   - vapnik sa dostane na myozinove vlanka, tie kvapkovite vybezky sa vstycia a tym padom prichytia na aktin, horcik robi presny opak - kvapkovite vybezky sa upokoja a padnu dole
   - serca protein uprace vapnik naspat do sarkoplazmatickeho retikula

8. **Na ktoré skupiny delíme svaly hlavy?** <br>
   Mimické a žuvacie

9. **Ktorý sval tvorí reliéf ramena?** <br>
   deltový?????????

10. **Ktoré svaly sa zúčastňujú pri vytváraní brušného lisu?** <br>
    priamy sval brucha, priecny sval brucha, vonkajsi sikmy sval brucha, vnutorny sikmy sval brucha, linea alba

11. **Vymenuj svaly hornej končatiny.** <br>

- svaly pletenca: deltovy sval
- svaly ramena: predná skupina ohýbačov, zadná skupina vystieračov
- svaly predlaktia: predná skupina ohýbačov, zadná skupina vystieračov
- svaly ruky

12. **Vymenuj svaly dolnej končatiny.** <br>

- svaly pletenca: bedrové svaly, sedací sval
- svaly stehna: predné vystierače: krajčírsky sval, štvorhlavný sval; vnútorné priťahovače: dlhý priťahovač; zadne ohybace: dvojhlavy sval
- svaly predkolenia: predný píšťalový; trojhlavý sval lýtka
- svaly nohy

13. **Vymenuj svaly chrbta.** <br>
    lichobeznikovy sval, nasirsi sval chrbta, ramenny sval hlavy, ramenny sval krku, mensi kosostvrocovy sval, vacsi kosostvorcov sval, zadny horny pilovity sval

14. **Vymenuj svaly krku.** <br>
    nadjazylkove svaly, podjazylkove svaly, kyvac hlavy, sikme svaly krku

15. **Vymenuj svaly trupu (chrbát, hrudník, brucho, panvové dno)** <br>
    **Chrbat:** lichobeznikovy sval, nasirsi sval chrbta, ramenny sval hlavy, ramenny sval krku, mensi kosostvrocovy sval, vacsi kosostvorcovy sval, zadny horny pilovity sval <br>
    **Hrudnik:** Deltovy sval, velky prsny sval, maly prsny sval, predny pilovity sval <br>
    **Brucho:** velky prsny sval, predny pilovity sval, vonkajsi sikmy sval brucha, linea alba, priamy sval brucha, priecny sval brucha, vonkajsi sikmy sval brucha, priamy sval brucha, vnutorny sikmy sval brucha <br>
    **Panvove dno:** Ischiocavernosus, bulbospongiosus, urethra (len zeny), vagina (len zeny), transverse perineal muscles, external anal sphincter, Levator ani, coccyx, gluteus maximus, penis (len muzi), <br>

16. **Na aké skupiny delíme svaly podľa funkcie? Napíš k nim aspoň 1 príklad.** <br>
    Ohybace (biceps), vystierac (triceps), pritahovac (žuvací - priťahuje sánku), odtahovac (svaly krku - odťahujú sánku), stlacovace (dlan), napinace (brucho), zvierace (analny zvierac)

17. **Ppíš antagonistický a synergický pohyb (prácu) svalov na konkrétnom príklade.** <br>
    **Antagonisticky:** jednym pohybom ruky jeden sval uvolnis, druhy napnes: biceps, triceps <br>
    **Synergicky:** to je také že pracujú všetky spolu na jednej činnosti ja neviem napríklad že brušné svaly? pracujú všetky na tom aby stiahli brucho a nie že jeden sa uvoľní a druhý stiahne

18. **Ktorá časť svalu je tzv. hlava a tzv. bruško svalu? Napíš príklad (dvojhlavého, trojhlavého a štvorhlavého svalu).** <br>
    Hlava: horny upon svalu, Chvost: dolny upon svalu, Brusko: telo svalo <br>
    Dvojhlavy: biceps, Trojhlavy: triceps, Stvorhlavy: kvadriceps

19. **Aký význam má vápnik a horčík pre svaly (a)? Je vhodné tieto minerálne látky užívať ako doplnky výživy (b)? Kedy a ako poznáme, že dané minerály nie sú v tele v rovnováhe? (c)** <br>
    a) vapnik: sposobuje kontrakciu, horcik: sposobuje uvolnenie (relaxaciu) <br>
    b) Tak asi hej? <br>
    c) mali by byť 1:1, príznaky že nie sú: únava, malátnosť, srdcové arytmie,
    príliš veľa horčíka: môžeme byť apatickí (nestiahneš sval) ale je to veľmi raritné, príliš veľa vápnika: svalové kŕče

20. **svalovica**
    anaeróbne cvičenie, srdce nedokáže pumpovať toľko okysličenej krvi (tuším kyslíkový dlh či ako sa to volá), svalové bunky musia dýchať anaeróbne, kyselina pyrohroznová sa štiepi na laktát kyseliny mliečnej = kvasenie, z toho vzniká bolesť, KTORÁ NIE JE SVALOVICA (ZDROJ MARTIN, KT. SYPE) <br>
    svalovica vznika prostym natrhnutim svalov - tym, ze sa natrhnu a potom znova zrastu, zvacsia sa
