# Telové tekutiny
1. **Napíš, ktoré zložky krvi zabezpečujú transport kyslíka a ktoré oxidu uhličitého.** <br>
<ins>kyslík</ins>: viaže sa na HEM<br>
<ins>oxid uhličitý</ins>: viaže sa na globín

2. **Pojmy: hematokrit, hemolýza, homeostáza, hemostáza, hemoglobín.** <br>
hematokrit -  podiel červených krviniek v celkovom objeme krvi <br>
hemolýza - rozpad červených krviniek sprevádzaný uvoľňovaním hemoglobínu <br>
homeostáza - stav dynamickej funkčnej rovnováhy v živom organizme, stálosť vnútorného prostredia, stav, pri kt. vnútorné prostredie živého organizmu zostáva v určitých limitoch, kt. umožňujú jeho normlálne fungovanie<br>
hemostáza - zastavenie krvácania <br>
hemoglobín - červené krvné farbivo v červených krvinkách s obsahom železa a bielkovín

3. **Prečo sa hematokrit zvyšuje pri pobyte človeka vo vysokých nadmorských výškach?** <br>
zvyšovanie hematokritu - vo vyšších nadmorských výškach je redší vzduch, na doplnenie dostatočného množstva kyslíka je potrebných viac červených krviniek

4. **Popíšte zloženie krvnej plazmy.** <br>
- Ide o žltkastú viskóznu tekutinu
- Tvorí 55% krvi
- 92% krvnej plazmy je voda, zvyšok sú org. a anorg. látky
- org. látky: bielkoviny, cukry, tuky, dusíkaté látky: močovina, kyselina močová, keratín, trieda čpavok poznáme??, aminokyseliny, plazmatické bielkoviny: albumíny, globulíny (alfa, beta, *gama*, do piče chémia zas kurva), fibrinogén
- anorg. látky: Na, K, Ca, Mg, Cl (tvorba membránového potencialu [čo to kurva je síce netuším, ale znie to múdro :D] a pri zrážaní krvi)

5. **Popíšte funkciu albumínov a globulínov.** <br>
Zas otázka z chémie? Krista boha do piče už som fakt nasraný, kurva! <br>
- albumíny: pre organizmus sú zdrojom aminokyselín
- globulíny: delia sa na niekoľko frakcií, gama-globulín: ochranná funkcia, imunoglobulíny - protilátky, ktoré dokážu po opakovanom styku s antigénom vyvolať rýchlu a cielenú obranu organizmu

6. **Pojmy: oxyhemoglobín, karbaminohemoglobín, karboxyhemoglobín, methemoglobín.** <br>
oxyhemoglobín: krvné farbivo hemoglobín presýtené kyslíkom, ktorý sa prenáša do tkanív <br>
karbaminohemoglobín: krvné farbivo hemoglobín, na kt. je naviazaný oxid uhličitý <br>
karboxyhemoglobín: krvné farbivo hemoglobín, na kt. je naviazaný oxid uhoľnatý <br>
methemoglobín: forma hemoglobínu, ktorá vzniká, keď je krv vystavená pôsobeniu dusičnanov a dusitanov, dochádza k oxidácii železa z Fe2+ na Fe3+ (normálne má hemoglobín Fe2+)

7. **Vymenujte typy bielych krviniek a určte ich fuknciu.** <br>
Biele krvinky majú obrannú funkciu. <br>
Delenie podľa štrktúry cytoplazmy:
- <ins>granulocyty</ins>:  
    - Neutrofilné (57% - 67%) – diapedéza, fagocytóza
    - Eozinofilné (2% - 5%) – alergické a parazitické ochorenia, protizápalové reakcie, diapedéza, fagocytóza
    - Bazofilné (1%) – alergické reakcie, uvoľňujú histamín
- <ins>agranulocyty</ins>: 
    - Monocyty (3% - 5%) – diapedéza, fagocytóza
    - Lymfocyty T, B (24% - 40%) – imunitné reakcie, tvorba protilátok <br>

- <ins>fagocytóza</ins>: pohlcovanie cudzorodého materiálu 
- <ins>diapedéza</ins>: prestupovanie bielych krviniek neporušenou stenou vlásočníc (kapilár) do medzibunkových priestorov

8. **Popíšte všetky fázy zástavy krvácania.**
    1. vazokonstrikcia - cieva sa zúži
    2. krvné doštičky narážajú a lepia sa na poranené miesto
    3. zmenia svoj tvar (zaguľatia sa a vytvoria výbežky)
    4. zhlukujú sa
    5. fibrinogén sa naviaže na doštičky a rozpadnú sa, vypúšťajú do okolia zmes chemických látok, ktoré spolu reagujú za prítomnosti iónov vápnika
    6. nastáva premena plazmatického (neaktívneho) protrombínu na (aktívny) trombín
    7. z rozpustnej plazmatickej bielkoviny fibrinogénu sa enzymatickou premenou stáva nerozpustný vláknitý fibrín, ktorý vytvorí hustú sieť nepriepustnú pre krvné častice, čím vznikne krvný koláč
    8. krvný koláč sa zmršťuje a poranená cievna stena sa sťahuje
    9. keď sa otvor v poranenej cieve zahojí, zrazenina sa rozpustí a odstráni 

9. **Vysvetlite význam aglutinogénu a aglutinínu v systéme AB0.** <br>
- aglutinogén: antigén
- aglutinín: protilátka <br>
Podľa aglutinogénu a aglutinínu vieme zistiť, akú má človek krvnú skupinu.
![MarineGEO circle logo](https://cdn.discordapp.com/attachments/693092604145041429/1105547235355275394/proxy-image.png) <br>
"každá krvná skupina má v krvnej plazme protilátky (aglutiníny) proti iným krvným skupinám. Napr. krvná skupina 0 má protilátky antiA aj antiB. Krvná skupina AB nemá v krvnej plazme žiadne protilátky. Aglutinogén je tzv. "označenie" červených krviniek, nachádza sa na membráne červených krviniek. Aglutinogény sa volajú aj antigény. Napr. krvná skupina A ma antigén A na membráne krvinky."<br>
~ Soňa

10. **Vysvetlite rozdiel medzi tkanivovým mokom a lymfou.**
- <ins>tkanivový mok</ins>
    - tvorí mimobunkové prostredie (prostredie medzi bunkami)
    - vzniká ultrafiltráciou krvnej plazmy – vo vlásočniciach (kapilárach)
    - zabezpečuje výmenu látok medzi krvou – bunkami alebo lymfou (nadbytok tkanivového moku)
- <ins>lymfa = miazga</ins> 
    - mliečne biela telová tekutina nachádzajúca sa v miazgových (lymfatických) cievach
    - vzniká v medzibunkových priestoroch z tkanivového moku a zbiera sa do miazgových vlásočníc
    - potom pokračuje ďalej širšími cievami, kde sa v rôznych úrovniach nachádzajú lymfatické uzliny, ako akési „filtračné stanice“
    - lymfatické cievy sa postupne spájajú do tzv. ductus thoracicus, ktorý lymfu odvádza do žilového systému
    - lymfa teda necirkuluje v uzavretom obehu, ako je to napríklad pri krvi

## Srdce a cievy
1. **Pojmy (zas): myokard, perikard, chlopňa, systola, diastola.** <br>
<ins>myokard</ins>: srdcová svalovina <br>
<ins>perikard</ins>: tenkostenný vakovitý útvar, v kt. je uložené srdce, osrdcovník <br>
<ins>chlopňa</ins>: svalový výbežok v srdci, cievach ap., kt. uzatvára al. uvoľňuje priechod a umožňuje prietok tekutiny iba jedným smerom <br>
<ins>systola</ins>: stiahnutie srdcovej komory, pri ktorom je krv vytláčaná do krvného obehu
<ins>diastola</ins>: ochabnutie svaloviny srdcovej komory po sťahu, op. systola

2. **Popíšte prevodový systém srdca.** <br>
- sťah predsiení
- ochabnutie komôr - naplnenie komôr krvou
- sťah komôr - z pravej do pľúc, z ľavej do tela
- do pravej predsiene z tela, do ľavej z pľúc
    - prevodový systém srdca - autonómny
- cievy, ktoré privádzajú odkysličenú (modrú) krv z tela do srdca: pľúcnica, dolná a horná dutá žila
- cievy, ktoré privádzajú okysličenú (červenú) krv z pľúc do srdca a tela: pľúcne žily, aorta (artéria)
- chlopne: štruktúry v srdci, kt. zabraňujú spätnému toku krvi

![MarineGEO circle logo](https://cdn.discordapp.com/attachments/1103258350688415754/1106288766299164722/image.png)

3. **Popíšte stavbu srdca aj s cievami.** <br>
<ins>pravá strana zhora (u teba):</ins> horná dutá žila, pľúcne žily, pravá predsieň, trojcípa chlopňa, pravá komora, spodná dutá žila <br>
<ins>ľavá strana zhora (u teba)</ins>: aorta, pľúcnica, ľavá predsieň, dvojcípa chlopňa, ľavá komora, srdcová svalovina, polmesiačikoviá chlopňa

4. **Napíšte, ktoré rizikové faktory vyvolávajú artériosklerózu.** <br>
- ukladanie tukových látok do stien tepny = cievy strácajú elasticitu, sú zhrubnuté a stvrdnuté
- rizikové faktory: cholesterol
- môže nastať: infarkt, mozgová príhoda

5. **Napíšte, ako sa volajú cievy, ktoré vyživujú srdce. Čo sa stane, ak sa zúžia alebo zablokujú?** <br>
- vencovité (koronárné) tepny
- ak sa upchajú, vzniká srdcový infarkt

6. **Napíšte, aké sú ideálne hodnoty krvného tlaku.**
- optimálna hodnota:
    - systolický tlak (100-120 torrov)
    - diastolický tlak (60-80 torrov)
- hypertenzia: zvýšený krvný tlak 
- hypotenzia: znížený krvný tlak

7. **Optimálna tepová frekvencia srdca v pokoji a pri športe.**
- v pokoji: 60-90 bpm, u športovcov menej
- pri aktivite: max 220-vek bpm

### Otázky, ktoré neviem:
- Telové tekutiny: 1, 3, 7, 9
- Srdce a cievy: 2, 4

